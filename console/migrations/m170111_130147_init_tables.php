<?php

use yii\db\Migration;

class m170111_130147_init_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%area}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'image' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(''),
            'weight' => $this->integer()->notNull()->defaultValue('0'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

        $this->createIndex('weight_idx',"{{%area}}",'weight');

        $this->createTable('{{%show}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'image' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(''),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);

        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'show_id' => $this->integer()->notNull(),
            'area_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
        $this->createIndex('show_id_idx',"{{%event}}",'show_id');
        $this->createIndex('area_id_idx',"{{%event}}",'area_id');
        
        $this->addForeignKey("area_fk", "{{%event}}", "area_id", "{{%area}}", "id", 'CASCADE');
        $this->addForeignKey("show_fk", "{{%event}}", "show_id", "{{%show}}", "id", 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%area}}');
        $this->dropTable('{{%show}}');
        $this->dropTable('{{%event}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
