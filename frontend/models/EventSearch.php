<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use common\models\Event;

/**
* EventSearch represents the model behind the search form about `common\models\Event`.
*/
class EventSearch extends Event
{
    public $show;
    public $area;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['id', 'show_id', 'area_id'], 'integer'],
            [['date','show','area'], 'safe'],
        ];
    }


    /**
    * @inheritdoc
    */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * Creates data provider instance with search query applied
    *
    * @param array $params
    *
    * @return ActiveDataProvider
    */
    public function search($params, $area_id=null)
    {
        $query = Event::find();
        
        // add conditions that should always apply here
        $query->joinWith(['area', 'show']);
        $query->filterWhere(['>','event.date', new Expression('NOW()')]);
        if(!is_null($area_id))
            $query->andFilterWhere(['event.area_id'=>$area_id]);
        $query->orderBy('date ASC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event.id' => $this->id,
            'event.date' => $this->date,
        ])
        ->andFilterWhere(['like', 'show.title', $this->show])
        ->andFilterWhere(['like', 'area.title', $this->area]);

        return $dataProvider;
    }
}
