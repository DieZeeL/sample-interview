<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Area;
use frontend\models\AreaSearch;
use frontend\models\EventSearch;
use yii\data\ActiveDataProvider;

/**
* Event controller
*/
class AreaController extends Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','slug'],
                'rules' => [
                    [
                        'actions' => ['index','slug'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
    * @inheritdoc
    */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
    * Displays homepage.
    *
    * @return mixed
    */
    public function actionIndex()
    {
        $searchModel = new AreaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionSlug($slug)
    { 
        $model = Area::find()->where(['slug'=>$slug])->one();
        if (!is_null($model)) {
            $searchModel = new EventSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$model->id);

            return $this->render('view', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);      
        } else {
            return $this->redirect('/area/index');
        }
    }
}