<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use common\components\ImgHelper

?>

<div class="col-md-4 event-item">
        <?php if($model->show->image && ImgHelper::exist($model->show->image)): ?>
            <?=Html::img(Yii::$app->urlManager->baseUrl . '/image/'.$model->show->image, ['alt'=>$model->show->image, 'class'=>'img-responsive'])?>
        <?php else : ?>
            <img class="img-responsive" src="http://placehold.it/700x400" alt="">
        <?php endif;?>
        
    <h3>
        <?=$model->show->title;?>
    </h3>
    <h4>
        <?= Html::encode($model->date) ?>
    </h4>
    <p><?=Html::a(Html::encode($model->area->title), Url::to(['area/slug', 'slug' => $model->area->slug]))?></p>
    <p><?= HtmlPurifier::process(StringHelper::truncateWords($model->show->description,20)) ?></p>
</div>

