<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Events
            <small>List</small>
        </h1>
    </div>
</div>
<!-- /.row -->

    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($model, $key, $index, $widget) use($dataProvider){
            if($index == 0){
                echo '<!-- Events Row -->';
                echo '<div class="row">';
            }    
            if($index != 0 && $index % 3 == 0  && ($index) != $dataProvider->count){   
                echo '</div>';
                echo '<!-- /.row -->';
                echo '<!-- Events Row -->';
                echo '<div class="row">';
            }
            echo Yii::$app->controller->renderPartial('_post',['model'=>$model]);
            if(($index+1) == $dataProvider->count){
                echo '</div>';
                echo '<!-- /.row -->';
            }       
        },
    ]);
    ?>
