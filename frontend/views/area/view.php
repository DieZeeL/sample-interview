<?php
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use common\components\ImgHelper;

/* @var $this yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Jumbotron Header -->
<header class="jumbotron hero-spacer">
    <div class="image pull-left" style="width:65%">
    <?php if($model->image && ImgHelper::exist($model->image)): ?>
        <?=Html::img(Yii::$app->urlManager->baseUrl . '/image/'.$model->image, ['alt'=>$model->image, 'class'=>'img-responsive','data-scale'=>'0.38'])?>
        <?php else : ?>
        <img class="img-responsive"  src="http://placehold.it/900x350" alt="" >
    <?php endif;?>
    </div>
    <h2><?=$model->title;?></h2>
    <p><?= HtmlPurifier::process(StringHelper::truncateWords($model->description,20)) ?></p>
    </p>
</header>

<hr>

<!-- Title -->
<div class="row">
    <div class="col-lg-12">
        <h3>Events</h3>
    </div>
</div>
<!-- /.row -->

<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model, $key, $index, $widget) use($dataProvider){
        if($index == 0){
            echo '<!-- Areas Row -->';
            echo '<div class="row text-center">';
        }    
        if($index != 0 && $index % 3 == 0  && ($index) != $dataProvider->count){   
            echo '</div>';
            echo '<!-- /.row -->';
            echo '<!-- Areas Row -->';
            echo '<div class="row  text-center">';
        }
        echo Yii::$app->controller->renderPartial('/event/_post',['model'=>$model]);
        if(($index+1) == $dataProvider->count){
            echo '</div>';
            echo '<!-- /.row -->';
        }       
    },
]);
?>
