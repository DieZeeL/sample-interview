<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use common\components\ImgHelper

?>

<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <?php if($model->image && ImgHelper::exist($model->image)): ?>
            <?=Html::img(Yii::$app->urlManager->baseUrl . '/image/'.$model->image, ['alt'=>$model->image, 'class'=>'img-responsive'])?>
            <?php else : ?>
            <img class="img-responsive" src="http://placehold.it/800x500" alt="">
        <?php endif;?>
        <div class="caption">
            <h3><?=$model->title;?></h3>
            <p class=""><?= HtmlPurifier::process(StringHelper::truncateWords($model->description,20)) ?></p>
            <p>
                <?=Html::a(Html::encode('Show Events'), Url::to(['area/slug', 'slug' => $model->slug]),['class'=>'btn btn-default'])?>
            </p>
        </div>
    </div>
</div>

