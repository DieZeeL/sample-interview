<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Show]].
 *
 * @see Show
 */
class ShowQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Show[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Show|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
