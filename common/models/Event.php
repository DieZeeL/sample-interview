<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
* This is the model class for table "event".
*
* @property integer $id
* @property string $date
* @property integer $show_id
* @property integer $area_id
* @property integer $created_at
* @property integer $updated_at
*
* @property Area $area
* @property Show $show
*/
class Event extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return 'event';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['date', 'show_id', 'area_id'], 'required'],
            [['date'], 'safe'],
            [['show_id', 'area_id', 'created_at', 'updated_at'], 'integer'],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
            [['show_id'], 'exist', 'skipOnError' => true, 'targetClass' => Show::className(), 'targetAttribute' => ['show_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'show_id' => 'Show',
            'area_id' => 'Area',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'show' => 'Show',
            'area' => 'Area',
        ];
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getShow()
    {
        return $this->hasOne(Show::className(), ['id' => 'show_id']);
    }

    /**
    * @inheritdoc
    * @return EventQuery the active query used by this AR class.
    */
    public static function find()
    {
        return new EventQuery(get_called_class());
    }
}
