<?php

namespace common\models;

use Yii;
use Zelenin\yii\behaviors\Slug;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $description
 * @property integer $weight
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Event[] $events
 */
class Area extends \yii\db\ActiveRecord
{
    public $uploadImage;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'image'], 'required'],
            [['description'], 'string'],
            [['weight', 'created_at', 'updated_at'], 'integer'],
            [['title', 'slug', 'image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['uploadImage'], 'safe'],
            [['uploadImage'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general. 
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
            TimestampBehavior::className(),
            'sortable' => [
                'class' => 'sjaakp\sortable\Sortable',
                'orderAttribute' => 'weight',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'image' => 'Image',
            'description' => 'Description',
            'weight' => 'Weight',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['area_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return AreaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AreaQuery(get_called_class());
    }
}
