<?php

namespace common\components;

use Yii;

class ImgHelper
{
    public static function exist($file) {
        return file_exists(Yii::getAlias('@uploadDir') . DIRECTORY_SEPARATOR . $file);
    }
}