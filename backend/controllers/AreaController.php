<?php

namespace backend\controllers;

use Yii;
use common\models\Area;
use backend\models\AreaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
* AreaController implements the CRUD actions for Area model.
*/
class AreaController extends Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete','order'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
    * @inheritdoc
    */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
    * Lists all Area models.
    * @return mixed
    */
    public function actionIndex()
    {
        $searchModel = new AreaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Area model.
    * @param integer $id
    * @return mixed
    */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new Area model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate()
    {
        $model = new Area();

        if ($model->load(Yii::$app->request->post())) {
            if($image = UploadedFile::getInstance($model, 'uploadImage')){
                $ext = end((explode(".", $image->name)));
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                $path = Yii::getAlias('@uploadDir') . DIRECTORY_SEPARATOR . $model->image;
                if(!file_exists(dirname($path)))
                    mkdir(dirname($path), 0777, true); 
            }
            if($model->save()){
                if($image)
                    $image->saveAs($path);
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Area model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id
    * @return mixed
    */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($image = UploadedFile::getInstance($model, 'uploadImage')){
                $ext = end((explode(".", $image->name)));
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";
                $path = Yii::getAlias('@uploadDir') . DIRECTORY_SEPARATOR . $model->image;
                if(!file_exists(dirname($path)))
                    mkdir(dirname($path), 0777, true);     
            }
            if($model->save()){
                if($image)
                    $image->saveAs($path);
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing Area model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $id
    * @return mixed
    */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionOrder( )   {
        $post = Yii::$app->request->post( );
        if (isset( $post['key'], $post['pos'] ))   {
            $this->findModel( $post['key'] )->order( $post['pos'] );
        }
    }

    /**
    * Finds the Area model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Area the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
        if (($model = Area::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
