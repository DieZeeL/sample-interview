<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
* Image controller
*/
class ImageController extends Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
    * @inheritdoc
    */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
    * Displays Image.
    *
    * @return string
    */
    public function actionIndex($file)
    {
        $path = Yii::getAlias('@uploadDir') . DIRECTORY_SEPARATOR . $file;

        if (file_exists($path))
        {
            header('Content-Type: image/jpeg');
            header('Content-Length: ' . filesize($path));
            readfile($path);
        }
        else
        {
            return "";
        }
    }

}
