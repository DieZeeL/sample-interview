<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\grid\GridView;
use sjaakp\sortable\SortableGridView;
use common\models\Area;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AreaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Areas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="alert alert-info">
        <strong>drag&drop</strong> enabled for change show order :)
    </div>

    <p>
        <?= Html::a('Create Area', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'orderUrl' => ['order'],
        'filterModel' => $searchModel,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'weight',
            'title',
            'slug',
            'image',
            [
                'attribute'=>'description',
                'value' => function (Area $data) {
                    return StringHelper::truncateWords($data->description,20);
                },
                'format' => 'html',
                'contentOptions' => ['style'=>'white-space: normal;'],
            ],
            [
                'attribute'=>'created_at',
                'value' => function (Area $data) {
                    return ($data->created_at == 0)?'':date('Y-m-d H:i:s',$data->created_at);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
