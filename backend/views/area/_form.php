<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Area */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'] 
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php 
    $pluginOptions = [
        'allowedFileExtensions'=>['jpg','gif','png'],
        'showPreview' => true,
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => false
    ];
    $options = ['accept' => 'image/*']; 
    if($model->image){
        $options['value'] = $model->image;
        $pluginOptions['initialPreview']=[
            Yii::$app->urlManager->baseUrl . '/image/'.$model->image,
        ];
        $pluginOptions['initialPreviewAsData']=true;
        $pluginOptions['initialCaption']=$model->image;
        $pluginOptions['overwriteInitial']=true;
    }
    ?>
    <?= $form->field($model, 'uploadImage')->widget(FileInput::classname(), [
        'options' => $options,
        'pluginOptions'=>$pluginOptions,
    ]); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'weight')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
