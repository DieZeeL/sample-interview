<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\grid\GridView;
use common\models\Show;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ShowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shows';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="show-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Show', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'slug',
            'image',
            [
                'attribute'=>'description',
                'value' => function (Show $data) {
                    return StringHelper::truncateWords($data->description,20);
                },
                'format' => 'html',
                'contentOptions' => ['style'=>'white-space: normal;'],
            ],
            [
                'attribute'=>'created_at',
                'value' => function (Show $data) {
                    return ($data->created_at == 0)?'':date('Y-m-d H:i:s',$data->created_at);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
