<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Show */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Shows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="show-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'slug',
            [
                'attribute'=>'image',
                'value' =>  Html::img(Yii::$app->urlManager->baseUrl . '/image/'.$model->image, ['alt'=>$model->image, 'class'=>'thing', 'height'=>'100px']),
                'format' => ['raw'],
            ],
            'description:ntext',
            [
                'attribute'=>'created_at',
                'value' =>  ($model->created_at == 0)?'':date('Y-m-d H:i:s',$model->created_at),
                'format' => 'raw',
            ],
            [
                'attribute'=>'updated_at',
                'value' =>  ($model->updated_at == 0)?'':date('Y-m-d H:i:s',$model->updated_at),
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
