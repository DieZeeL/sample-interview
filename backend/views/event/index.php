<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\Event;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date',
            [
                'attribute'=>'show',
                'value' => function (Event $data) {
                    return Html::a(Html::encode($data->show->title), Url::to(['/show/view', 'id' => $data->show->id]));
                },
                'format' => 'raw',

            ],
            [
                'attribute'=>'area',
                'value' => function (Event $data) {
                    return Html::a(Html::encode($data->area->title), Url::to(['/area/view', 'id' => $data->area->id]));
                },
                'format' => 'raw',
            ],
            [
                'attribute'=>'created_at',
                'value' => function (Event $data) {
                    return ($data->created_at == 0)?'':date('Y-m-d H:i:s',$data->created_at);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
