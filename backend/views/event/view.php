<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            [
                'attribute'=>'show',
                'value' =>  Html::a(Html::encode($model->show->title), Url::to(['/show/view', 'id' => $model->show->id])),
                'format' => 'raw',

            ],
            [
                'attribute'=>'area',
                'value' =>  Html::a(Html::encode($model->area->title), Url::to(['/area/view', 'id' => $model->area->id])),
                'format' => 'raw',
            ],
            [
                'attribute'=>'created_at',
                'value' =>  ($model->created_at == 0)?'':date('Y-m-d H:i:s',$model->created_at),
                'format' => 'raw',
            ],
            [
                'attribute'=>'updated_at',
                'value' =>  ($model->updated_at == 0)?'':date('Y-m-d H:i:s',$model->updated_at),
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
