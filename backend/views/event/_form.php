<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\datetime\DateTimePicker;
use yii\web\JsExpression;
use common\models\Show;
use common\models\Area;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DateTimePicker::className(),[
        'type' => DateTimePicker::TYPE_INPUT,
        'value' => date('Y-m-d h:i:s',time()),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh:ii:ss'
        ]
    ]); ?>
    <?php 
    $showOne = empty($model->show_id) ? '' : Show::findOne($model->show_id)->title; 
    ?>
    <?= $form->field($model, 'show_id')->widget(Select2::classname(), [
        'initValueText' => $showOne,
        'options' => ['placeholder' => 'Select a Show ...'],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 0,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['show-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(show) { return show.title; }'),
            'templateSelection' => new JsExpression('function (show) { return show.title; }'),
        ],
    ]); ?>
    
    <?php $areaOne = empty($model->area_id) ? '' : Area::findOne($model->area_id)->title; ?>
    <?= $form->field($model, 'area_id')->widget(Select2::classname(), [
        'initValueText' => $areaOne,
        'value' => 'rrr',
        'options' => ['placeholder' => 'Select Area ...'],
        'pluginOptions' => [
            'allowClear' => false,
            'minimumInputLength' => 0,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => \yii\helpers\Url::to(['area-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(area) { return area.title; }'),
            'templateSelection' => new JsExpression('function (area) { return area.title; }'),
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
