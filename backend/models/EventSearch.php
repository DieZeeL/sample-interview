<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;

/**
* EventSearch represents the model behind the search form about `common\models\Event`.
*/
class EventSearch extends Event
{
    public $show;
    public $area;
    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['id', 'show_id', 'area_id', 'created_at', 'updated_at'], 'integer'],
            [['date','show','area'], 'safe'],
        ];
    }


    /**
    * @inheritdoc
    */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * Creates data provider instance with search query applied
    *
    * @param array $params
    *
    * @return ActiveDataProvider
    */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here
        $query->joinWith(['area', 'show']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $dataProvider->sort->attributes['area'] = [
            'asc' => ['area.title' => SORT_ASC],
            'desc' => ['area.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['show'] = [
            'asc' => ['show.title' => SORT_ASC],
            'desc' => ['show.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event.id' => $this->id,
            'event.date' => $this->date,
            'event.created_at' => $this->created_at,
            'event.updated_at' => $this->updated_at,
        ])
        ->andFilterWhere(['like', 'show.title', $this->show])
        ->andFilterWhere(['like', 'area.title', $this->area]);

        return $dataProvider;
    }
}
